Personal Name Extractor (PNE) is a Java 1.8 API designed to locate personal names
(the names of people) in a specified block of text. It is an extension of a Software Engineering
project at Old Dominion University, where the original assignment was assigned by Dr. Steven J. Zeil
( http://cs.odu.edu/~zeil/ ). 

PNE makes use of a WEKA ( https://weka.wikispaces.com/ ) learning machine in order to evaluate
each word within the loaded block of text, and determine whether or not a given word is a personal name.

//TODO Add more information, give details regarding API usage.